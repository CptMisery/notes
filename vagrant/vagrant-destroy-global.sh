#!/usr/bin/env bash
# Created: 2016-11-07
# Author: Juan Borla <jaborla@gmail.com>
# License: WTFPL-2.0 (`./vagrant-destroy-global.sh-LICENSE`)
#
# Notes: All comments added by Robert Robinson <robr3rd@gmail.com> per chat...
# ...messages from Juan (the original author).

# This script (which should be run on the Vagrant host) pulls a list from...
# ...Vagrant of all boxes that currently exist on the host. It then iterates...
# ...over that list and runs `vagrant destroy` on each one, one at a time.
#
# Intended use case: You know that you have Vagrant boxes lying around eating...
# ...up your resources, but are unsure of where they are and just want them...
# ...gone.

# WARNING: This script may crash if a 7-digit hex code is used as a box name.

for x in $(vagrant global-status)
do
	# `${x:4}` exists because `vagrant global-status` prefixes each code with...
	# ...color coding for some reason.
	if [[ ${x:4} =~ [a-f0-9]{7}$ ]]; then
		echo "vagrant destroy ${x:4} -f"

		# Optional: Safety can be improved by removing `-f` from the below line.
		# Notably, this negates the desired "fire-and-forget" functionality.
		vagrant destroy ${x:4} -f
	fi
done
