# Vim

## Resources
- [A Great Vim Cheat Sheet](http://vimsheet.com/)

## Multi-line editing
1. Enter "visual block mode" (`Ctrl`+`v`)
1. Enter "visual edit mode" (`I` / `Shift`+`i`)
1. Make the desired changes at the location of the current cursor
	- Yes, it isonly making the changes in that one location. Yes, that is the intended functionality (see next bullet point).
	- Vim stores these changes in some sort of "internal buffer" or something, waiting to unleash them upon the rest of the selection upon your command (i.e. #4).
1. Press "escape" (`ESC`) to apply your changes to the entire selection.
