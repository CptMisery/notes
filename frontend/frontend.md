```sass
// Syntax: `.[context-]component[--state] {}`

// All components are self-contained. These may be 'simple' (such as a content component)
// or 'complex' (such as a structural component CONTAINING one or more 'simple' components).
// 
// In summary, a "simple component" MAY or MAY NOT be a part of a "complex component", but
// a "complex component" MUST contain one or more "simple components".


// Naming convention
.block_name-element_name--modifier {} // simple

// multiple simples
.button {} // simple
.link {} // simple


// Accordion
.accordion {} // complex
.accordion-item {} // simple
.accordion-title {} // simple
.accordion-content {} // simple


// Navigation
.nav {} // complex
.nav-item {} // simple
.nav-item--isActive {} // simple (+state)


// Header
.header { // complex
	background-color: gray;

	.logo {} // simple
	.button {} // simple

	// complex
	.nav {}
}

.header-title {} // simple
```
