#! /usr/bin/env bash
#
# This script removes any `phpdoc` folder in the current directory (should one exist) and creates a new one
#
# Run from directory with what you want documented
rm -rf phpdoc && phpdoc -d . -t phpdoc -i ignored/path1,ignored/path2/file1.ext,ignored/path3
