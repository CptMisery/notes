#! /usr/bin/env bash
#
# Syntax: new-sftp-user [username] [password]
# This file is included under /usr/bin as "new-sftp-user" and can be run like any other Shell command.
#
# Although the following "mkdir" could be done in the useradd command below with the "-m" flag,
# that uses the standard skeleton directory structure and gives the new user ownership.
# So it ends up being MORE work to delete and change permission on those after the fact than it is to just create it as root

mkdir /home/$1
useradd -g sftpgroup -d /home/$1 $1 # useradd -g [group] -d [home_directory] [user_name]
yes $2 | passwd $1 # Spam the passwd prompt for the new password with "$2" (our password argument) | `yes` auto-responds to every shell prompt with the specified string
mkdir /home/$1/uploads # This will be the directory that the user/group has access to
chgrp sftpgroup /home/$1/uploads # All SFTP users are in this group
chmod 775 /home/$1/uploads # The group needs write access (I think g+w is another way to write this, but best to be explicit)
