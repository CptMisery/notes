# Subversion (SVN)

## Creating a new repository
```bash
# Make a home for all of your Subversion projects
mkdir -p /var/svn/repos
cd /var/svn/repos


# Create the currently-desired project
svnadmin create [project-name]


# Configure proper permissions to the contents of the repo for easy administration
chgrp -R svn /var/svn/repos/[project-name]/code # In this example, the `svn` user group is the gateway for access
chmod -R 775 /var/svn/repos/[project-name]/code


# Import existing codebase to Subversion
svn import code [original code location] -m 'initial import'
```



## Access Management
> Note: Assumption is made here that `svnserve` is serving Subversion over the `svn+ssh://` prefix and not, say, Apache on `http://`.

Access Management is fairly straightforward as it simply leverages SSH accounts.

### Setup
```bash
groupadd svn # Control access via the "svn" user group (for simplicity)
```

### Adding an account
```bash
useradd -M -g svn -c "John Doe" jdoe
```



## Serving Subversion
There are a number of ways to serve up a Subversion repository. Some methods are solid long-term solutions while others are "good enough" temporary solutions.

### `svnserve`
This is a "good enough" temporary Subversion serving solution.

> Note: The only way to access this is with the `svn+ssh://` protocol, which requires on-server SSH accounts.

Due to file permissions issues (e.g. allowing `svn` users to write in `/var/svn/repos`), do this:

```bash
#!/usr/bin/env sh
# File: /var/svn/svnwrapper.sh
# Source (modified): http://www.startupcto.com/server-tech/subversion/setting-up-svn

umask 002 # Set the `umask` such that files are group-writable

# Call the 'real' `svnserve`, also passing in the default repo location
exec /usr/bin/svnserve "$@" -r /var/svn/repos
```

...once that file is created...:

```bash
ln -s /var/svn/svnwrapper.sh /usr/local/bin/svnserve # Make this script more "important" than the non-`local/` one

sudo svnserve -d # Execute `/usr/local/bin/svnserve` (due to the `$PATH` prioritization)
```

### Others
Other options are available, but I haven't had a reason to do those yet; therefore, I have yet to write documentaiton for doing so.

Worth noting, though, is that nearly every other option is better than `svnserve` -- for instance, serving with Apache over the (simpler) `http://` protocol.



## Test/Confirm
```bash
svn co svn+ssh://robr3rd@[server]/[project-name] # Do a test checkout to ensure functionality
```



## Converting a Subversion repository to Git
1. Install this tool: https://github.com/nirvdrum/svn2git
	- Note: It must be run on Linux (as of this writing).
1. Run `svn2git file:///var/svn/repos/[project-name]/code`
	- There are numerous ways of running this depending on how the Subversion repository is setup, but if it follows the defaults then you need only tell `svn2git` where to find it and it will do the rest.
	- The output of this command will be all of the same metadata and files, but in Git format -- including all commits and history.
1. `git clone --bare <filepath of svn2git output>`
	- This makes a **bare** Git glone of the project, which is what we want for just a repo host.
1. Once the new Git repository is setup, run the following commands:
	1. `git remote add origin <new repo URL>` 
	1. `git push --all`
	1. `git push --tage`
1. Done!

If still worried:

1. Clone the new Git repo.
1. Ensure that all files exist:
	- Run `diff` between it and the original code (this would highlight any missing files).
1. Ensure that all commits/tags/history exist:
	- Compare histories with `git log` and `svn log`
