# Security

## General
- [OWASP - Browse by Technology](https://www.owasp.org/index.php/Category:Technology)
- Akamai
	- [`[state of the internet]` quarterly reports](https://www.akamai.com/us/en/our-thinking/state-of-the-internet-report/global-state-of-the-internet-security-ddos-attack-reports.jsp)


## Web Applications
- OWASP
	- [Secure Coding Practices - Quick Reference Guide](https://www.owasp.org/index.php/OWASP_Secure_Coding_Practices_-_Quick_Reference_Guide)
	- [Gossary of Attack Types](https://www.owasp.org/index.php/Category:Attack)


## Pentesting
- [OWASP OWTF](https://www.owasp.org/index.php/OWASP_OWTF)
	- OWASP Offensive Web Testing Framework is a project focused on penetration testing efficiency and alignment of security tests to security standards like: The OWASP Testing Guide (v3 and v4), the OWASP Top 10, PTES and NIST.


## Passwords
TL;DR = Use `bcrypt`. PBDF2 is less preferred, but fine.

The longer version = do some research:

- https://news.ycombinator.com/item?id=2716714
- http://stackoverflow.com/questions/1561174/sha512-vs-blowfish-and-bcrypt
- https://medium.com/@mpreziuso/password-hashing-pbkdf2-scrypt-bcrypt-1ef4bb9c19b3
- https://security.stackexchange.com/questions/4781/do-any-security-experts-recommend-bcrypt-for-password-storage
- https://news.ycombinator.com/item?id=3724560
- https://news.ycombinator.com/item?id=2004962
- http://stackoverflow.com/questions/40566966/bcrypt-vs-pbkdf2-for-encrypting-private-keys
