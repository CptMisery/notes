# Sublime Text

## Plugins
1. Alignment
1. Color Highlighter
1. CSSComb
1. DocBlockr
1. EasyDiff
1. Emmet
1. Gist
1. Git
1. GitGutter
1. GotoDocumentation
1. Grunt
1. Gulp
1. LocalHistory
1. Markdown Preview
1. Package Control
1. Pandoc
1. PHP Code Sniffer
1. PHPUnit Completions
1. PHPUnit
1. Pretty JSON
1. Pretty YAML
1. Side Bar
1. SublimeCodeIntel
1. SublimeLinter
1. SublimeREPL
1. Unquote
1. Vagrant
1. Xdebug
