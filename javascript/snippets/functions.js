/**
 * Mimics PHP's number_format() function.
 *
 * Formats a number to have two decimal places, and commas at every 3 digits (like US currency).
 *
 * @author Robert Robinson <robr3rd@gmail.com>
 *
 * @param  {int|float|string} $input_value Any numerical value
 * @return {float}                         That same numerical value, but with commas and decimal places
 */
function number_format($input_value) {
	return parseFloat($input_value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Essentially an "Undo" function for number_format().
 *
 * Strips out any commas and/or dollar signs -- made so that math operations can be done on "pretty formatted" numbers since special characters make the calculations incorrect
 *
 * @author Robert Robinson <robr3rd@gmail.com>
 *
 * @param  {int|float|string} $input_value Anything with commas and dollar signs that need to be separated out
 * @return {float}                         The $input_value, but just as a plain number (no commas or dollar signs)
 */
function un_number_format($input_value) {
	return parseFloat($input_value.toString().replace(/,/g, '').replace(/\$/g, ''));
}
