# MySQL - InnoDB

## Converting to InnoDB "file per table"
1. `mysqldump` all databases into a single file (e.g. `data.sql`).
1. Drop all databases (except for the `mysql` schema).
1. Run `mysql -uroot -p -Ae"SET GLOBAL innodb_fast_shutdown = 0;" && systemctl stop mysqld`.
1. Modify `/etc/my.cnf` to have the content just below this list.
1. Delete `ibdata1`, `ib_logfile0`, and `ib_logfile1`.
	- Verify that `/var/lib/mysql`'s only content is the `mysql` schema.
1. `systemctl restart mysqld`
	- Note: This will re-create the 3 files that were deleted earlier at sizes of roughly 10MB, 1GB, and 1GB, respectively. This is **okay** and is by design.
1. Re-import the `data.sql` file.
	- `ibdata1` will grow, but will only contain table metadata. Each InnoDB table will exist outside of `ibdata1`.

```ini
;/etc/my.cnf

[mysqld]
innodb_file_per_table
innodb_flush_method=O_DIRECT
innodb_log_file_size=1G
innodb_buffer_pool_size=4G
;Sidenote: Whatever your set for innodb_buffer_pool_size, make sure innodb_log_file_size is 25% of innodb_buffer_pool_size.
```

### New structure
> Suppose you have an InnoDB table named mydb.mytable. If you go into /var/lib/mysql/mydb, you will see two files representing the table
>
> `mytable.frm` (Storage Engine Header)
> `mytable.ibd` (Home of Table Data and Table Indexes for mydb.mytable)
> `ibdata1` will never contain InnoDB data and Indexes anymore.
> 
> With the `innodb_file_per_table` option in `/etc/my.cnf`, you can run `OPTIMIZE TABLE mydb.mytable` and the file `/var/lib/mysql/mydb/mytable.ibd` will actually shrink.

