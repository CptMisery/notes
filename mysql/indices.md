# MySQL Indices
When making changes to table indices in MySQL, there are often quite a few considerations that one must bear in mind.

A few examples:

- Large tables (i.e. many records)
	- If the software using the target table only performs `INSERT` and `SELECT` operations, the index can be applied on a _duplicate_ of table (see "duplicate table" methodology for an example).
	- If `UPDATE` queries are run on a table, then the index changes MUST be done on the live instance of the table.
- Small tables (i.e. many records)
	- It's generally okay to apply index changes on live tables if they are sufficiently small in size / record quantity.

## Duplicate Table
```sql
USE database;

SHOW TABLE STATUS; # Lists every table and some apporximate information (including record count, albeit not in real-time)

SHOW INDEX FROM `foo`; # View *existing* indices on the target table

CREATE TABLE `foo_new` LIKE `foo`; # Copies the structure but no data

CREATE INDEX `idx_foo_column1_column2` ON `foo_new` (`column1`, `column2`);

INSERT INTO `foo_new` SELECT * FROM `foo` WHERE `id` >= 1 AND `id` < 4000000; # Proceed in controlled incremenets while monitoing server availability/resource usage, as this allows for quick and convenient shutoff + resting periods

INSERT INTO `foo_new` SELECT * FROM `foo` WHERE `id` >= 999 AND `id` < 9999; RENAME TABLE `foo` TO `foo_old`; RENAME TABLE `foo_new` TO `foo`;
```
