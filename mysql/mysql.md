# MySQL
## Cheat Sheets
- [Reserved Words](https://dev.mysql.com/doc/refman/en/keywords.html)

## Guides / Learning Resources
- [MySQL for Beginners](https://www.ntu.edu.sg/home/ehchua/programming/sql/MySQL_Beginner.html)
- [Basic overview of RDBMS design](https://www.ntu.edu.sg/home/ehchua/programming/sql/Relational_Database_Design.html)
- [Sample Databases](https://www.ntu.edu.sg/home/ehchua/programming/sql/SampleDatabases.html)
- [Intermediate Techniques](https://www.ntu.edu.sg/home/ehchua/programming/sql/MySQL_Intermediate.html)
- [Miscellaneous Notes](https://www.ntu.edu.sg/home/ehchua/programming/sql/SQLMisc.html)
	- "Such as `COUNT` does not count the `NULL` value. You can use `SUM(IF(test, 1, 0))` for selected count.""

## Alternatives
A few alternatives exist for MySQL (excluding Oracle). In the case of the two below, they both began life as MySQL forks and continue to do so to this day, so any upstream fixes are free.

- [MariaDB](https://mariadb.com/kb/en/mariadb/what-is-mariadb-53/#query-optimizer)
	- Community-driven with a focus on speed and features, leaving full MySQL compatibility behind (if migrating FROM Maria TO MySQL) in acceptance of being the only way if features are desired.
	- Nearly every Linux distro is now using this as the default install even though the package may still be labelled as `mysql`.
- [Percona Server](https://www.percona.com/software/mysql-database/percona-server/feature-comparison)
	- Company-driven with a focus on speed, stability, and maximal compatibility with MySQL (due to the nature of their customers generally being corporations).
	- Percona develops a multitude of MySQL tools, which have deep integrations with Percona Server.
