# MySQL CLI Administration
## Connecting
To connect to MySQL, you must pass in the username and password of the user with which you'd like to connect.

> Note: If only passing those two, the host of `localhost` is assumed, so be sure that `'[user]'@'localhost'` exists in your MySQL instance.

```shell
mysql --user=robr3rd --password
```

While the above uses the "long form" (and more self-documenting) parameters, the following "shorthand" version is generally the more common of the two:

```shell
mysql -urobr3rd -p
```

Using either command, MySQL will prompt you for your password upon execution. This is done in this manner so as to ensure that the password is never in the STDIN history.

Alternatively, the password _can_ be passed directly into the command:

```shell
mysql -urobr3rd -pMyAwesomePassword
```

...but then anybody who runs `history` for the user that executed the command will see that MySQL user's password.

One other password entry method that is worth noting depending on your use case is that of adding it in the same manner as above, but doing so as the output of a subcommand.

```shell
mysql -urobr3rd -p`cat /usr/share/file-whose-contents-is-the-password-and-has-appropriate-permissions-configured`
```



## Backup and Restore / Import
### Backup
#### Full Backup
> Note: The timestamp is *not* necssary of course; rather, it is simply a (rather helpful) suggestion.

```shell
mysqldump -urobr3rd -p db1 > db1-$(date +%Y-%m-%d_%H:%M:%S).dump.sql 
```

#### Schema Backup (i.e. "Create a Migration")
```bash
mysqldump --no-data -urobr3rd -p db1 > db1-$(date +%Y-%m-%d_%H:%M:%S).schema.sql
```

#### Data Backup (i.e. "Create a Seed")
```bash
mysqldump --no-create-info -urobr3rd -p db1 > db1-$(date +%Y-%m-%d_%H:%M:%S).data.sql
```

### Restore / Import
To "restore" data or schema (or _anything_ from a `*.sql` file that defines data or the structure of your database), you start by simply connecting to MySQL the same way that you normally would (`mysql -h [host] -u [user] -p [database]`), but then the contents of the backup get piped into the connection command.

```shell
mysql -urobr3rd -p db1 < db1.[timestamp].dump.sql
```

> Note: This should generally be performed on a _fresh_ database. For instance, after `DROP DATABASE db1` and `CREATE DATASE db1` if the desire is to fully reset _everything_.



## Process Management
### Kill Processes
This all essentially revolve around running `SHOW PROCESSLIST;`, grabbing a process ID, and running `KILL [process id]`. Although this works great in situations where only a handful of proceses need to be killed, they are quite cumbersome when the task involves killing off, say, 30 processes. For that, these exist.

#### Step 1: Build a "kill file"
Kill all proceeses spawned from a specific host (the `%` here matches the wildcard port that each connection may use):

```sql
SELECT CONCAT('KILL ',id,';') AS run_this FROM information_schema.processlist WHERE host LIKE '192.168.1.100%' INTO OUTFILE '/tmp/kill-processes.sql';
```

Kill processes per user:

```sql
SELECT CONCAT('KILL ',id,';') AS run_this FROM information_schema.processlist WHERE user = 'root' INTO OUTFILE '/tmp/kill-processes.sql';
```

Note: You should also be able to kill based on the query depending on your use case.

#### Step 2: Run the "kill file"
For best results, run immediately after creation (in case process IDs change, fall off, etc., since that'll throw an error and halt execution).

```bash
mysql -urobr3rd -p < /tmp/kill-processes.sql
```
