# MySQL Monitoring
## Basic, Live Monitoring
To monitor the MySQL process list in real-time in a manner similar to how `top` would be used, run the following command:

```shell
watch --differences "mysql -urobr3rd -p -e'SHOW FULL PROCESSLIST'"
```

This can be further improved by adding the following snippet into `.zshrc` (or equivalent config file for other shells) which will add support for arbitrary MySQL connection parameters:

```shell
# Usage: `mysqltop -urobr3rd -pSecretPassword -h example.com -P 3307`
function mysqltop() {
    MYSQL_OPTS=$@
    watch --differences "mysql $MYSQL_OPTS -e 'show full processlist'"
}
```

([source](http://naleid.com/blog/2009/06/18/poor-mans-top-for-mysql))

## Better Monitoring
```shell
watch -n1 --differences 'uptime; free -m; mysqladmin status; mysqladmin processlist;'
```
