# WorkFlowy
> Referral code (we both get 2x the storage as a normal free user): [https://workflowy.com/invite/34a282b3.lnx](https://workflowy.com/invite/34a282b3.lnx)

## Naming convention
A few simple rules for symbol usage:

- `@` is a special symbol
- `#` is a special symbol
- There doesn't appear to be a difference between the two, so it's up to the user to crate their own rules for differentiation
- Neither accepts a number as the first character of their value, so for instance `#fourth` is valid but `#4th` is not and won't be indexed.
- **Everything** is searchable...but the above two symbols are searchable _faster_ and render differently on the frontend.


### Others' Examples
> https://blog.workflowy.com/2015/11/10/dee-jay-docs-action-flow/


### My Convention
My _real_ global `[index]` standards definition (as of 2017-04-08):

- Key: Use contexts (`@`) for "things" (e.g. people/companies/locations) and tags (`#`) for abstract concepts (e.g. time/status/priority).
	- Tip: Both contexts and tags must start with a letter; numbers and symbols will not work.
- **[priority]** #A #B #C [...] #A1 #D23
- **[people]** @me
- **[location]** @home @work @fun
- **[time]** #now #soon #later #d-yyyy-mm-dd_hh:mm
	- Each piece of the timestamp (#d-yyyy-mm-dd_hh:mm) is optional starting from the right. For instance: #d-yyyy-09 is valid, as is #d-2016-mm-dd_10
- **[status]** #ready #active #doing #done #blocked #follow-up #future
- **[calendar]** #spring #summer #fall #autumn #winter #jan #feb #mar #apr #may #jun #jul #aug #sep #oct #nov #dec


### My Convention (Explained)
1. People and Places = `@[name of person/place]`
	- I abbreviate where possible to initials or nicknames of some sort. I generally stick to whatever somebody's email address is in a professional setting (usually "first letter of first name, then last name") and in personal maybe a nickname.
	- In each section, I try to maintain an Index of its contents--specifically of the people and some general "profile notes" about them.
		- This makes it trivial to see them mentioned in a complex note, click on their name, and read their "profile" easily, plus aids in WF's autocomplete.
1. Concepts, Projects, "Abstract Things" in general = `#[thing]`
1. Dates follow the following naming convention: `#d-[yyyy-mm-dd_hh:mm]`
	- _(Note: because of the rule about `#` not allowing numbers immediately after, the initial `#d-` basically serves to "typecast" the proceeding string to 'datetime'.)_
	- In the case of dates, the important part is that the left-most section is always required, with anything to the right being additional. Examples:
		- `#d-2017`
		- `#d-2017-04`
		- `#d-2017-04-08_19:30`
	- The reason for this is pretty logical and is supplementally backed up by how WF's search inherently works:
		- You may not always recall what day something fell on, but you'll almost certainly know the year, and if you know the _year_ then you may very well know the _year and month_ as well!
		- WF's search is iterative, so if you start with the "big" section and then drill down from there it's more likely to work with you rather than against you.
	- "But what if I _don't_ know the year", you may be thinking...
		- Well, in that case I refer you to item #2 about "abstract things/concepts"--specifically, I'd suggest that you do `#d-2017-04-08_19 #spring`
	- "But I have an edge case in which something really _is_ a datetime of sorts but it happens on the 3rd of every month at 10pm!"
		- This methodology works for that, too! As mentioned above the string format is required left-to-right, but in your very particular scenario the year and month _don't matter_! So here's how you'd write that: `#d-yyyy-mm-03_22:00`. Yep! That would be the _literal letters `yyyy-mm` rather than placeholders! So when you've got something that happens every year in July, you now know that you can write it using this system as follows: `#d-yyyy-07`!

