# Git

## Commit Messages
### Do TODOs belong in Commit Messages?
Very much no, and this makes perfect sense when you think about "What is a commit message?"

A commit message is a human-readable description that is designed to answer the question of "what are these changes doing?". A "commit", itself, is a diff of changes that were made vs what was there before.

When you think about it like this (i.e. "This is what was done") then a TODO (i.e. "This is what I _will_ do") has no place in a commit message--one is past-tense while the other is future-tense.

"TODO"s belong in a ticketing system, not a changelog (because they're not changes yet).



## Submodules
To pull only a epcific path from within a submodule, use the following template in your project's `.gitmodules` file:

```
[submodule "magento/magento2"]
	path = PHP_CodeSniffer/magento2
	url = https://github.com/magento/magento2.git
```

In this example, we are calling the `magento/magento2` repository, but only pulling the `path`. The end result of this is that we're pulling `github.com/magento/magento2.git/PHP_CodeSniffer/magento2`.



## Removing accidental whitespace (from a Pull Request)
If the commit was the most recent commit in the branch, then obviously `git reset --hard HEAD^1` is simpler and much more straight-forward; however, if your situation is more complex than that -- with errant whitespace changes strewn across multiple commits, but intermingled with REAL work that you would still like to keep -- then an appropriately more complex solution is necessary. Specifically, something like the following:

```bash
git reset --soft HEAD~1 # checkout the previous commit (and move the `diff` to the current working branch)
git diff -w origin/master > /tmp/commit.patch # `-w` means "ignore whitespace"...this is comparing what we checked out vs what's in our working branch
git reset --hard HEAD # blow away our working branch (which still includes whitespace)
# Note: We are now prior to when we foolishly made whitespace changes
# Now make the whitespace changes (e.g. start saving files with EditorConfig or apply other similar rulesets) and `commit` it all as a "whitespace changes" commit
git apply /tmp/commit.patch # apply the non-whitespace changes that we identified from our `git diff -w` earlier
git add . -A
git commit -v -a # the `-v` (verbose...shows the diff of what you'll be committing) is optional and only exists to demonstrate and verify that this process worked
# [copy/paste your old commit message]
git push --force # because we just rewrote this branch's history
rm /tmp/commit.patch # clean up
```



## What changed in that location between now and then?
`git diff [--flags] ["from" tag, branch, or commit hash] HEAD [filepath scope of the diff]`

For `[--flags]`, see these potential candidates to help tailor the output to your needs:

- `--shortstat` shows total number of files changed along with total additions and total deletions
	- Example: `1 file changed, 5 insertions(+), 6 deletions(-)`
- `--short` shows list of files changed with summary of additions, deletions, and changes per file + the output of `--shortstat` (similar to `git status`)
	- Example: `path/to/file.extension | 11 +++++------` + `--shortstat`
- `--minimal` provides a more traditional `diff` appearance much like a `patch` file
