# Monitoring

## Single Application Server
### `load_monitor.sh`
```shell
#!/usr/bin/env bash
echo "loadavg, mysql-cpu, apache-threads, apache-mem, apache-avg-mem, mysql-mem, mysql-threads" >> measure_new.txt
while true
do
	echo $(date "+%Y-%m-%d %H:%M:%S"; echo ","; cat /proc/loadavg | awk '{print $1","$2","$3","}'; top -b -n 1 -p 21903 | tail -n 2 | head -n 1 | awk '{print $9","}'; ps -ylC httpd --sort:rss | awk '{sum+=$8; ++n} END {print n","sum","sum/n}'; ps -ylC mysqld --sort:rss | awk '{sum+=$8} END {print ","sum}'; mysqladmin status -uadmin -p`cat /etc/psa/.psa.shadow` | awk '{print ","$4}') >> measure_new.txt
	sleep 30
done
```

## Centralized Logging with ELK (Elasticsearch, Logstash, Kibana)
(source: https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elk-stack-on-ubuntu-14-04#install-logstash)
