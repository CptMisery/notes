# RESTful API
There will be inevitable crossover with nearly every other "web" section. Crossover will be kept brief and to the point and a reference link should be used in the place of long-form explanations.


## HTTP Methods
> `GET` - Retrieve resources
> `POST` - Create resources
> `PUT` - Update whole resources
> `PATCH` - Updates pieces of resources
> `DELETE` - Delete resources
>
> -Adapted from http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html


## HTTP Status Codes
> Here are some of the most used, in my humble opinion :) 
>
> - 2xx - Success
> 	- 200 OK
> 	- 201 Created
> 	- 203 Partial Information
> 	- 204 No response
> - 3xx - Redirection
> 	- 301 Moved
> 	- 302 Found
> 	- 304 Not Modified
> - 4xx / 5xx - Error
> 	- 400 Bad Request
> 	- 401 Unauthorized
> 	- 402 Payment Required
> 	- 403 Forbidden
> 	- 404 Not Found	
> 	- 500 Internal Server Error	
> 	- 503 Service Unavailable
>
> -http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html


## Idempotency
> No matter how many times you call `GET` on the same resource, the response should always be the same and no change in application state should occur.
>
> Idempotent methods: `GET`, `PUT`, `OPTIONS`
> Non Idempotent methods: `POST`
>
> What about the method `DELETE`? If you `DELETE /accounts/4402278` twice...:
> 1. The accounts will not be deleted many times ... thinking this way it is idempotent
> 1. The second time the resource will not be found and should return a 404 Not found, this way it's not idempotent anymore
>
> -http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html
