# API

## JSON API
There is not a singular, official standard; however, there are quite a few unofficial specs.

- [JSON API](http://jsonapi.org/format/) ([examples](http://jsonapi.org/examples/)) is likely the most "official-esque".
	- Unlike ___ which establishes a bare minimum and leaves the rest up to implementation, this one leaves less to the imagination (which is, of course, sometimes a good thing) by outlining a number of additional common features used by APIs and how those should be implemented.
	- Good choice if: You need a standard that is well-documented and leaves little room from inconcsistencies between developers. This may be a good fit for larger teams or for teams that cannot otherwise agree on a standard (since this one spells it all out).
- [JSend](https://labs.omniti.com/labs/jsend) is a far more basic specification that places a certain level of responsibility on the developers. Implementation is also quicker/easier and (due to the flexibility) often a more "natural"-feeling fit to most projects.
	- Good choice if:
		- A large degree of flexibility is needed.
		- There is a consensus on code standards and there is very little risk for developers to stray from that (since the freedom that this affords is both beautiful but also risky for consistency)
		- Bolting on _after_ something has been created that may not be well-architected (which is where flexibility comes in)
		- You just need something simple to get the job done with a minimal learning curve.
- RESTful URIs: http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html
