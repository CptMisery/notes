# Standards
Various standards that I often find myself referring to (well, Googling desperately for since--apparently!--they aren't something that the general populace of Earth seek out given how difficult they are to find sometimes).

## Internationalization
- [ISO 3166-1 - Country Codes](https://en.wikipedia.org/wiki/ISO_3166-1)
	- According to the ISO 3166 Maintenance Agency (ISO 3166/MA), the only way to enter a new country name into ISO 3166-1 is to have it registered in one of the following two sources:
		1. United Nations Terminology Bulletin Country Names
			- To be listed in the bulletin Country Names, a country must be at least one of the following:[7]
				- A member state of the United Nations
				- A member of one of its specialized agencies
				- A party to the Statute of the International Court of Justice
		1. Country and Region Codes for Statistical Use of the UN Statistics Division.
	- Specifications include:
		- [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) (`alpha-2` denotes "2 character" so that each abbreviation is only 2 characters in length)
			- `US` = United States
			- `SE` = Sweden
			- `VN` = Viet Nam
				- Anecdotally, yeah it apparently really **is** _"Viet Nam"_ (two words) and not _"Vietnam"_, which is acknowledged on Wikipedia in this note: "ISO country name follows UN designation (common name: Vietnam)" ([source](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2))
		- [ISO 3166-1 alpha-3](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)
			- `USA` = United States of America
			- `SWE` = Sweden
			- `VNM` = Viet Nam
		- [ISO 3166-1 numeric](https://en.wikipedia.org/wiki/ISO_3166-1_numeric)
			- `840` = United States of America
			- `752` = Sweden
			- `704` = Viet Name
- [ISO 4217 - Currency Codes](https://en.wikipedia.org/wiki/ISO_4217)
	- Examples:
		- Traditional currencies
			- `USD` = United States dollar (United States)
			- `AUD` = Australian dollar (Australia)
			- `GBP` = Pound Sterling (United Kingdom)
			- `CHF` = Swiss franc (Switzerland)
		- Cryptocurrencies
			- `BTC` = Bitcoin (note: "BTC conflicts with ISO-4217 because BT stands for Bhutan.")
			- `LTC` = Litecoin
			- `ETH` = Ether (as in the "Etherium" Bitcoin competitor) (note: "ETH code conflicts with ISO-4217 because ET stands for Ethiopia.")
- [Country calling codes (phone)](https://en.wikipedia.org/wiki/List_of_country_calling_codes)

## Miscellaneous
- Barcodes
	- Note: It is safe to assume that all of this information came from [Wikipedia's "Universal Product Code"](https://en.wikipedia.org/wiki/Universal_Product_Code) page with some personal paraphrasing and summation of research from other sources by me throughout. A number of these points are direct quotes from Wikipedia although no direct quotes have been used from other sources.
	- Standards
		- EAN (**International Article Number** or **European Article Number**)
			- EAN is also sometimes written as EAN-13 to explicitly denote its 13-digit format.
			- EAN-8 is an 8-digit variation of the EAN barcode.
			- The EAN-13 was developed as a superset of UPC-A, adding an extra digit to the beginning of every UPC-A number. This expanded the number of unique values theoretically possible by ten times to 1 trillion. EAN-13 barcodes also indicate the country in which the company that sells the product is based (which may or may not be the same as the country in which the good is manufactured). The three leading digits of the code determine this, according to the `GS1` country codes. Every UPC-A code can be easily converted to the equivalent EAN-13 code by prepending 0 digit to the UPC-A code. This does not change the check digit. All point-of-sale systems can now understand both equally.
			- 2-digit EAN-2 and 5-digit EAN-5 are supplemental barcodes, placed on the right-hand side of EAN-13 or UPC. These are generally used for periodicals to indicate the current year's issue number, like magazines, or books, and weighed products like food (to indicate the manufacturer's suggested retail price (MSRP)).
			- UPC usage/compatibility/conversion notes:
				- All products, marked with an EAN, will be accepted in North America currently - in addition to those already marked with a UPC.
				- Product with an existing UPC does not have to be re-marked with an EAN.
				- In North America, the EAN adds 40% more codes, mainly by adding digits '10 through 12' to the UPC digits '00 through 09'. This is a powerful incentive to phase out the UPC.
			- Breakdown
				- GS1 Prefix ([List of GS1 Country Codes, Wikipedia](https://en.wikipedia.org/wiki/List_of_GS1_country_codes))
					- The first 3 digits - usually identifying the national GS1 Member Organization to which the manufacturer is registered (not necessarily where the product is actually made).
					- The GS1 Prefix is 978 or 979, when the EAN-13 symbol encodes a conversion of an International Standard Book Number (ISBN).[3] Likewise the prefix is 979 for International Standard Music Number (ISMN) and 977 for International Standard Serial Number (ISSN).
						- The EAN "country code" 978 (and later 979) has been allocated since the 1980s to reserve an Unique Country Code (UCC) prefix for EAN identifiers of published books, regardless of country of origin, so that the EAN space can catalog books by ISBNs[3] rather than maintaining a redundant parallel numbering system. Similar arrangements are in place for ISSNs for periodicals ("country code" 977) and ISMNs for sheet music (code 979-0).
					- The first three digits of the EAN-13 (GS1 Prefix) usually identify the GS1 Member Organization which the manufacturer has joined. Note that EAN-13 codes beginning with 0 are actually 12-digit UPC codes with prepended 0 digit. In the last few years, more products sold by retailers outside United States and Canada have been using EAN-13 codes beginning with 0, since they were generated by GS1-US.
				- Manufacturer Code
					- The manufacturer code is a unique code assigned to each manufacturer by the numbering authority indicated by the GS1 Prefix. All products produced by a given company will use the same manufacturer code. EAN-13 uses what is called "variable-length manufacturer codes." Assigning fixed-length 5-digit manufacturer codes, as the UCC has done until recently, means that each manufacturer can have up to 99,999 product codes--and many manufacturers don't have that many products, which means hundreds or even thousands of potential product codes are being wasted on manufacturers that only have a few products. Thus if a potential manufacturer knows that it is only going to produce a few products, EAN-13 may issue it a longer manufacturer code, leaving less space for the product code. This results in more efficient use of the available manufacturer and product codes.
					- In ISBN and ISSN, this component is used to identify the language in which the publication was issued and managed by a transnational agency covering several countries, or to identify the country where the legal deposits are made by a publisher registered with a national agency, and it is further subdivided any allocating subblocks for publishers; many countries have several prefixes allocated in the ISSN and ISBN registries.
				- Product Code
					- The product code is assigned by the manufacturer. The product code immediately follows manufacturer code. The total length of manufacturer code plus product code should be 9 or 10 digits depending on the length of country code (2-3 digits).
					- In ISBN and ISSN, it uniquely identifies the publication from the same publisher; it should be used and allocated by the registered publisher in order to avoid creating gaps; however it happens that a registered book or serial never gets published and sold.
				- Check digit
					- The check digit is an additional digit, used to verify that a barcode has been scanned correctly. It is computed modulo 10, where the weights in the checksum calculation alternate 3 and 1. In particular, since the weights are relatively prime to 10, the EAN-13 system will detect all single digit errors. It also recognizes 90% of transposition errors (all cases, where the difference between adjacent digits is not 5).
					- As with many other aspects of the EAN spec, EAN-13 uses the [ISBN-13 standard's check digit algorithm (Wikipedia)](https://en.wikipedia.org/wiki/International_Standard_Book_Number#ISBN-13_check_digit_calculation).
			- Encoding
				- The 13-digit EAN-13 number can be divided into 3 groups: first digit, left group of 6 digits, right group of 6 digits.
				- The barcode consists of 95 equally spaced areas (also called modules). From left to right:
				- 3 areas to encode the start marker.
				- 42 (6*7) areas making up the left group of 6 digits. This can be further subdivided into 6 subgroups, each consisting of seven areas. The subgroups encode digits 2-7. Each of these encodings can have even or odd parity. The parities taken together, indirectly encode the first digit of EAN-13.
				- 5 areas to encode the marker for the center of the barcode.
				- 42 (6*7) areas making up the right group of 6 digits. This can be further subdivided into 6 subgroups, each consisting of seven areas. The subgroups encode digits 8-13. Digits 8-13 are all encoded with even parity. Digit 13 is the check digit.
				- 3 areas to encode the end marker.
				- Each area can be black bar (1) or white space (0). A maximum of four black bar areas can be grouped together, these make up a wide black bar. Likewise a maximum of four white space areas can be grouped together, these make up a wide white space.
				- The start marker and the end marker are encoded as 101. The center marker is encoded as 01010.
				- Each digit in EAN-13 (except digit 1, which is not directly encoded) consists of seven areas. A decimal digit is encoded so that it consists of two (wide) bars and two (wide) spaces.
				- The digits in the left group are encoded so that they always start with a white space, and end with a black bar. The digits in the right group are encoded so that they always start with a black bar, and end with a white space.
				- Finally, the combination of variable-width black bars and white spaces encodes the EAN-13 number.
		- UPC (Universal Product Code)
			- The most common/primary UPC standards are UPC-A and UPC-E.
				- UPC-A is 12 digits
					- UPC-A: (10 possible values per left digit ^ 6 left digits) × (10 possible values per right digit ^ 5 right digits) = 100,000,000,000.
					- The visual "bar" format is `SLLLLLLMRRRRRRE` in which `S`tart, `M`iddle, and `E`nd are taller sets of bars (**"guard patterns"**) that act as separators of the numeric portion of the barcode (for instance, the `M` divides the 12 digits into two sets of 6).
				- UPC-E is 6 digits
					- To allow the use of UPC barcodes on smaller packages, where a full 12-digit barcode may not fit, a 'zero-suppressed' version of UPC was developed, called UPC-E, in which the number system digit, all trailing zeros in the manufacturer code, and all leading zeros in the product code, are suppressed (omitted). This symbology differs from UPC-A in that it only uses a 6-digit code, does not use `M`iddle "guard pattern".
					- UPC-E: (10 possible values per digit ^ 6 digits) × (2 possible parity patterns per UPC-E number) = 2,000,000.
			- There are a number of older patterns which are effectively all but phased out. Ongoing usage of these is discouraged. ("As the UPC becomes technologically obsolete, it is expected that UPC-B and UPC-C will disappear from common use by the 2010s. The UPC-D standard may be modified into EAN 2.0 or be phased out entirely.")"
				- UPC-B is a 12-digit version of UPC with no check digit, developed for the National Drug Code (NDC) and National Health Related Items Code.
				- UPC-C is a 12-digit code with a check digit.
				- UPC-D is a variable length code (12 digits or more) with the 12th digit being the check digit. These versions are not in common use.
				- UPC-2 is a 2-digit supplement to the UPC used to indicate the edition of a magazine or periodical.
				- UPC-5 is a 5-digit supplement to the UPC used to indicate suggested retail price for books.
			- Note: UPC-A 042100005264 is equivalent to UPC-E 425261 with the "EOEEOO" parity pattern, which is defined by UPC-A number system 0 and UPC-A check digit 4.
			- Below is description of all possible number systems with corresponding 12-digit UPC-A numbering schema **`L`LLLLLRRRRR`R`**, where **`L`** denotes number system digit and **`R`** check digit.
				- `0, 1, 6, 7, 8, 9`: For most products. The LLLLL digits are the manufacturer code (assigned by local `GS1` organization), and the RRRRR digits are the product code.
				- `2`: Reserved for local use (store/warehouse), for items sold by variable weight. Variable-weight items, such as meats, fresh fruits, or vegetables, are assigned an item number by the store, if they are packaged there. In this case, the LLLLL is the item number, and the RRRRR is either the weight or the price, with the first R determining which (0 for weight).
				- `3`: Drugs by National Drug Code(`NDC`) number. Pharmaceuticals in the U.S. use the middle 10 digits of the UPC as their `NDC` number. Though usually only over-the-counter drugs are scanned at point of sale, `NDC`-based UPCs are used on prescription drug packages and surgical products and, in this case, are commonly called `UPN Codes`.
				- `4`: Reserved for local use (store/warehouse), often for loyalty cards or store coupons.
				- `5`: Coupons. The LLLLL digits are the manufacturer code, the first 3 RRR are a family code (set by manufacturer), and the next 2 RR are a coupon code, which determines the amount of the discount. These coupons can be doubled or tripled.
			- Encoding:
				- The UPC-A barcode is visually represented by strips of bars and spaces, that encode the UPC-A 12-digit number. Each digit is represented by a unique pattern of 2 bars and 2 spaces. The bars and spaces are variable width, i.e. 1, 2, 3, or 4 modules wide. The total width for a digit is always 7 modules, consequently UPC-A 12-digit number requires a total of 7×12 = 84 modules.
				- A complete UPC-A is 95 modules wide:
					- 84 modules for the digits (L and R sections) combined with
					- 11 modules for the S (start), M (middle), and E (end) guard patterns.
				- The S (start) and E (end) guard patterns are 3 modules wide and use the pattern bar-space-bar, where each bar and space is one module wide.
				- The M (middle) guard pattern is 5 modules wide and uses the pattern space-bar-space-bar-space, where each bar and space is also one module wide.
				- In addition, a UPC-A symbol requires a quiet zone (extra space of 9 modules wide) before the S (start) and after the E (end) guard patterns.
				- The UPC-A's left-hand side digits (the digits to the left of the M (middle) guard pattern) have odd parity, which means the total width of the black bars is an odd number of modules, on the contrary, the right-hand side digits have even parity.
					- Consequently, a UPC scanner can determine whether it is scanning a symbol from left-to-right or from right-to-left (the symbol is upside-down).
					- After seeing a S (start) or E (end) guard pattern (they are the same, bar-space-bar, whichever direction they are read), the scanner will first see odd parity digits, if scanning left-to-right, or even parity digits, if scanning right-to-left. With the parity/direction information, an upside-down symbol will not confuse the scanner.
					- When confronted with an upside-down symbol, the scanner may simply ignore it (many scanners alternate left-to-right and right-to-left scans, so they will read the symbol on a subsequent pass) or recognize the digits and put them in the right order.
					- There is another property in the digit encoding. The right-hand side digits are the optical inverse of the left-hand side digits, i.e. black bars are turned into white spaces and vice versa.
						- For example, the left-hand side "4" is space×1 - bar×1 - space×3 - bar×2, ...
						- ...meanwhile the right-hand side "4" is bar×1 - space×1 - bar×3 - space×2.
			- Check digit
				- Generally, in the UPC-A system, the check digit is calculated as follows:
					1. Sum the digits at odd-numbered positions (first, third, fifth,..., eleventh).
					1. Multiply the result by 3.
					1. Add the digit sum at even-numbered positions (second, fourth, sixth,..., tenth) to the result.
					1. Find the result modulo 10 (i.e. the remainder, when divided by 10).
					1. If the result is not 0, subtract the result from 10.
				- For example, in a UPC-A barcode "03600029145x", where x is the unknown check digit, x can be calculated by:
					1. Sum the odd-numbered digits (0 + 6 + 0 + 2 + 1 + 5 = 14).
					1. Multiply the result by 3 (14 × 3 = 42).
					1. Add the even-numbered digits (42 + (3 + 0 + 0 + 9 + 4) = 58).
					1. Find the result modulo 10 (58 mod 10 = 8).
					1. If the result is not 0, subtract the result from 10 (10 − 8 = 2).
					1. Thus, the check digit x is 2.
