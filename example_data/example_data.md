# Example Data
"Safe" reserved example numbers for things such as Credit Cards and Social Security Numbers.

## SSN (Social Security Number)
### Decommissioned due to misuse
- 078-05-1120 (source: [Social Security Administration's post on misused SSNs that have had to be retired](https://www.ssa.gov/history/ssn/misused.html))
	- This one was a real one that was widely publicized and therefore replaced and placed on a "blacklist" of ones not to use again.
	- This one (subjectively) _looks_ the most real.
	- This one has been **officially** decommissioned.
	- This one, "is far and away the worst involving a real SSN and an actual person". (same source)
- 219-09-9999 (source: [Social Security Administration's post on misused SSNs that have had to be retired](https://www.ssa.gov/history/ssn/misused.html))
	- This was actually used as a "sample" _by the SSA_ in 1940.
	- This one has been **officially** decommissioned.
- 789-10-1112 (source: [Keybase docs](https://keybase.io/getting-started))
	- Note: 

> I can't find a reference to the 'advertisement' reserved SSNs on the SSA site, but it appears that no numbers starting with a 3 digit number higher than 772 (according to the document referenced above) have been assigned yet, but there's nothing I could find that states those numbers are reserved. [...] I think it would be nice to have a citation from the SSA, though I suspect that now that Wikipedia has made the idea popular that these number would now have to be reserved for advertisements even if they weren't initially.
> -[Stack Overflow](http://stackoverflow.com/a/2314156/2564560)

> - Numbers with all zeros in any digit group (000-xx-####, ###-00-####, ###-xx-0000).
> - Numbers with an area group (first three digits) of 666 or any of 900-999.
> - Numbers that have been misused in any way, such as the well known 078-05-1120.
> Consider using one of these (the obviously invalid 000-00-0000 would be a good one IMO).
> -[Stack Overflow](http://stackoverflow.com/a/2313726/2564560)
