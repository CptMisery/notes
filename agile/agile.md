# Agile

## Estimates
(source: https://www.axosoft.com/downloads/scrum-training/axosoft-scrum-in-6-steps.pdf also included as a sibling to this file)

When estimating work, use this standard (no estimations in-between):

- 1h
- 2h
- 4h
- 8h
- 2d
- 3d
- 5d
