# Linux - Debian
## Releases
(source: https://wiki.debian.org/DebianReleases)

| Version | Release Date | EOL Date   | Code Name  |
| ------- | ------------ | ---------- | ---------- |
|         |              |            | Bullseye   |
|         |              |            | Buster     |
|         |              |            | Stretch    |
| 8.0     | 2015-04-25   | 2018-06-06 | Jessie     |
| 7.0     | 2013-05-04   | 2016-04-26 | Wheezy     |
| 6.0     | 2011-02-06   | 2014-05-31 | Squeeze    |
| 5.0     | 2009-02-14   | 2012-02-06 | Lenny      |
| 4.0     | 2007-04-08   | 2010-02-15 | Etch       |
| 3.1     | 2005-06-06   | 2008-03-31 | Sarge      |
| 3.0     | 2002-07-19   | 2006-06-30 | Woody      |
| 2.2     | 2000-08-15   | 2003-06-30 | Potato     |
| 2.1     | 1999-03-09   | 2000-09-30 | Slink      |
| 2.0     | 1998-07-24   |            | Hamm       |
| 1.3     | 1997-07-02   |            | Bo         |
| 1.2     | 1996-12-12   |            | Rex        |
| 1.1     | 1996-06-17   |            | Buzz       |
| 0.93R6  | 1995-10-26   |            |            |
| 0.93R5  | 1995-03      |            |            |
| 0.91    | 1994-01      |            |            |
