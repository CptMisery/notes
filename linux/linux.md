# Linux

## Add new SSH user
$ useradd -m -s /bin/bash -c "John Doe" jdoe
 
$ mkdir /home/jdoe/.ssh
$ chmod 700 /home/jdoe/.ssh
$ nano /home/jdoe/.ssh/authorized_keys # Add public key signature
$ chown -R jdoe:$(groups jdoe | awk '{print $3}') # Owned by the user and their primary group ("as if" the created the directory)
  
### Set password
$ passwd jdoe
Changing password for user jdoe.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
  
### Force reset on first login
$ chage -d 0 jdoe


## Auto-mount on boot
Edit `/etc/fstab` (with care).

Notably, since "whitespace" is significant in `fstab` since it is treated as a value separator, remember that you MUST use `\040` instead when you need to represent a "space" character.

```
# For disks, the syntax is as follows:
# UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX 	[mount point]	[filesystem type]	[mount parameters]	[dump] [fsck order]
UUID=12345678-1234-1234-1234-123456789012	/	ext4	defaults	1 1
UUID=12345678-1234-1234-1234-123456789012	/boot	ext4	defaults	1 2
UUID=12345678-1234-1234-1234-123456789012	/home	ext4	defaults	1 3
UUID=12345678-1234-1234-1234-123456789012	/tmp	ext4	defaults,nosuid,nodev,noexec	1 4
UUID=12345678-1234-1234-1234-123456789012	swap	none	defaults	0 0

UUID=12345678-1234-1234-1234-123456789012	/run/media/Data	ext4	defaults	1 5


# For bindings--which are basically more low-level/"semi-permanent" symlinks that work via 'mount --bind' but happen automatically on startup--, the syntax is as follows:
# [from]	[to]	[filesystem type]	[mount parameters]	[dump] [fsck order]
/run/media/Data/robr3rd\040stuff/Music	/home/robr3rd/Music	none	defaults,bind	0 0
/run/media/Data/robr3rd\040stuff/Games	/home/robr3rd/Games	none	defaults,bind	0 0
/run/media/Data/robr3rd\040stuff/TV\040Shows	/home/robr3rd/TV\040Shows	none	defaults,bind	0 0
```
