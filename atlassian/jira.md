# Atlassian - JIRA

## Same-column status transition
It is currently not possible to drag-and-drop a Card from one Status to another if both Statuses belong to the same Column. This is a permitted function of JIRA, but the Board GUI does not currently offer this functionality.

There is already a (somewhat long-standing) ticket for this (and a brief discussion).

In the meantime, there are two ways to trigger the transition:

1. On the Board:
	1. Select the Card you'd like to affect.
	1. On your keyboard, press "." (period).
		- This is a shortcut for:
			1. Select Card
			1. Right-hand sidebar hit the "..." dropdown
			1. Choose "More Actions..."
	1. This opens a modal that offers type-to-search and lists every possible Workflow and Action that can happen to the Issue/Card.
	1. Type the name of the destination Status.
1. On the Issue's dedicated page:
	1. Select a Card.
	1. On the right sidebar, click on the Issue ID.
	1. This takes you to the Issue's dedicated page.
	1. Near the top, activate the "Workflow" dropdown.
	1. Choose the desired Status.



## Editing a Project Key
For the official documentation, see [Atlassian's entry about this](https://confluence.atlassian.com/adminjiracloud/editing-a-project-key-776636293.html).

A test that I once performed (2017-04-11) is as follows:

- Start with TEST
	- Create TEST-1
	- Create TEST-2
	- Create TEST-3
	- Create TEST-4
	- Create TEST-5
- Rename TEST -> TST
	- (result: looks like a project-wide redirect from TEST to TST is created, regardless of the ticket number…details below)
	- Create TST-6
	- Attempt to access TST-5 = Works
	- Attempt to access TST-3 = Works
	- Attempt to access TEST-5 = Works (redirects to TST-5)
	- Attempt to access TEST-3 = Works (redirects to TST-3)
	- Attempt to access TEST-6 = Works (redirects to TST-6)
- Try to create a project with the key of "TEST" to try to "steal" the redirect that's reserved = Denied, key reserved by TST.
- Rename TST -> TT
	- Create TT-7
	- Attempt to access TST-6 = Works (redirects to TT-6)
	- Attempt to access TST-2 = Works (redirects to TT-2)
	- Attempt to access TEST-6 = Works (redirects to TT-6)
	- Attempt to access TEST-2 = Works (redirects to TT-2)
	- Attempt to access TEST-7 = Works (redirects to TT-7)
	- Attempt to access TST-7 = Works (redirects to TT-7)
	- Attempt to access TT-7 = Works
	- Attempt to access TT-6 = Works
	- Attempt to access TT-5 = Works
	- Attempt to access TT-3 = Works
	- Attempt to access TT-2 = Works
- Try to create a project with key of "TST" = Denied
- Try to create a project with key of "TEST" (again, trying to 'steal' the key) = Denied, still reserved by TST.
- Delete TST
- Create project with TEST = Works (key no longer reserved)
- Create project with TST = Works (key no longer reserved)

