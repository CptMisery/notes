# Static Site Generators (a.k.a. SSGs)
Static Website Generators are systems that take a human-readable and usually non-markup set of tiles and from them create a static HTML website.

For instance, we might have a directory full of Markdown (`*.md`) files that houses its project's documentation. This is great and all, but now we want to build a website out of these so that it can be hosted somewhere and shared with the masses!

To accomplish this, we use an SSG tool--examples of these provided below--to iterate over our `/docs/` directory and generate a fully-functional HTML website + CSS that's usually themeable.

- [Jigsaw](http://jigsaw.tighten.com/)
	- Based on PHP and inspired by Laravel's Blade syntax, Jigsaw will feel right at home for PHP developers experienced with the Laravel framework. Includes support for Laravel's Elixir as well.
- [Jekyll](https://jekyllrb.com/)
	- Based on Ruby, Jekyll is arguably the de-facto standard that really set the standard for what sorts of things people have come to expect of SSGs.
	- Examples:
		- [Docker's documentation](https://github.com/docker/docker.github.io)
