# Laravel / Lumen
I see no reason to split these out into entirely different directory trees since nearly everything between the two of them is identical.

Instead, here's how this is structured:

- Assume Laravel for everything always.
- Anything that is Lumen-specific will either be called out right then and there, or for more general things, the `lumen.md` file adjacent to this one.


## Releases
Laravel is released twice a year -- one around June and one around the end of the year. All releases can be seen here: https://laravel.com/docs/5.4/releases. For LTS releases, bug fixes are provided for 2 years and security fixes for 3. For general releases, bug fixes are provided for 6 months and security fixes are provided for 1 year.


### Release Schedule
(source: https://laravel-news.com/release-cycle-changes)

- From Laracon 2013 through 2016-12-02 = June / December
- 2016-12-03+ = January / July
	- This change was made to allow for more testing time between Symfony releases + it better coincides with Laracon.


### Release History
| Version | Release Date      | Bug Fixes  | Security Fixes |
| ------- | ----------------- | ---------- | -------------- |
| V1      | 2011-06           |            |                |
| V2      |	2011-09           |            |                |
| v3      |	2012-02           |            |                |
| v4      |	2013-05           |            |                |
| v5      |	2015-02-04        | 2015-08    | 2016-02        |
| v5.1    | 2015-06-09 (LTS)  | 2017-06    | 2018-06        |
| v5.2	  | 2015-12-21        | 2016-06    | 2016-12        |
| v5.3	  | 2016-08-23        | 2017-02    | 2017-08        |
| v5.4	  | 2017-01-24        | 2017-07    | 2018-01        |
| v5.5	  | 2017-07 (LTS)     | 2019-07    | 2020-07        |



## Routing
### Names Routes
To use convenient aliases for your routes, check here: https://laravel.com/docs/5.2/routing#named-routes

The notable aspect to this is to just keep in mind that although the typical format is:

```php
<?php
Route::get('user/profile', [
    'as' => 'profile',
    'uses' => 'UserController@showProfile'
]);
```

...if you're using anonymous functions then you just drop the 'uses' key entirely, as the last item is then assumed to be an anonymous function:

```php
<?php
Route::get('user/profile', [
	'as' => 'profile',
	function () {
    	//
	}
]);
```
