# PhpStorm

## 'Getting Started' - especially from another editor
https://laracasts.com/series/how-to-be-awesome-in-phpstorm

## Getting Started with PHP_CodeSniffer
- https://confluence.jetbrains.com/display/PhpStorm/PHP+Code+Sniffer+in+PhpStorm

## Vagrantfile syntax highlighting
1. Download/`git clone` this repo: [ruby.tmbundle](https://github.com/textmate/ruby.tmbundle)
1. `Preferences` -> `Editor` -> `TextMate Bundles` -> `+` -> `Ruby` and set the path to wherever you're storing the files that you just downloaded
1. `Preferences` -> `Editor` -> `File Types` -> `File Supported Via TextMateBundles` -> Ensure that `Vagrantfile` is in the `Registered Patterns` section.

## Fonts
- Code
	- Primary font
		- Face: Fira Code
		- Size: 12
		- Line spacing: 1.3
	- Secondary Font
		- Face: Hack
		- Size: 12
		- Line spacing: 1.3
	- Tertiary Font
		- Face: Inconsolata-g for Powerline
		- Size: 12
		- Line spacing: 1.3

## Plugins
- .env files support
- .ignore
- AngluarJS
- Apache Config (.htaccess) support
- ASP
- Atlassian Connector for IntelliJ IDE
- BashSupport
- Behat Support
- Blade Support
- Codeception Framework
- CodeGlance
- CoffeeScript
- Color Ide
- Commad Line Tool Support
- Copyright
- CSS Support
- CVS Integration
- Database Tools and SQL
- Docket Integration
- Drupal Support
- EditorConfig
- File Watchers
- Gherkin
- Git Integration
- GitHub
- GNU GetText siles support (`*.po`)
- Google App Engine Support for PHP
- Haml
- hg4idea
- HTML Tools
- Ini4Idea
- IntelliLang
- JavaScript Debugger
- JavaScript Inetntion PowerPack
- JavaScript Support
- Joomla! Support
- Laravel Plugin
- Less support
- Markdown support
- NodeJS
- Perforce Integration
- Performance Testing
- Phing Support
- PHP Docker
- PHP Remote Interpreter
- PHPSpec BDD Framework
- Puppet Support
- QuirksMode
- R Language Support
- Raml Plugin for IntelliJ
- Refactor-X
- Remote Hosts Access
- REST Client
- ReStructuredText  Support
- Sass support
- scss-lint
- Settings Repository
- Subversion Integration
- Task Management
- Terminal
- TextMate bundles support
- Time Tracking
- tslint
- Twig Support
- UML Support
- Vagrant
- Vue.js
- W3C Validators
- WordPress Support
- XPathView + XSLT Support
- XSLT-Debugger
- YAML
- YAML/Ansible support
