# TickTick
Personal task list manager.

## Custom Style
The web interface is too bright with all of the available themes, and there is no "Dark" theme available as of this writing. As such, I've simply taken to overriding their styles with my own with Chrome's "StyleBot" extension using the following CSS (on top of the built-in "Black" ("Pure Color") theme):

```css
#add-task .input-like {
    background-color: rgba(245,245,250,0.5);
}

.dark-theme .g-left {
    background-color: #222;
}

.dark-theme .min-cal {
    background-color: #333;
}

.task {
    background-color: #eee;
}

.task.selected {
    background-color: #dadada;
    border-bottom: 1px solid #bbb;
    border-top: 1px solid #bbb;
}

.task:hover {
    background-color: #e3e3e3;
    border-bottom: 1px solid #ccc;
    border-top: 1px solid #ccc;
}

.td-body {
    background-color: #eee;
}

body, .g-center {
    background-color: #ddd;
}

div.editor-with-link {
    background-color: #eee;
}

div.task-progress-wrapper {
    background-color: #e3e3e3;
}
```

