# Apache
## Package Name
- In most Debian-based Linux distros (such as Ubuntu), the package is called `apache2` (which will presumably change once [the latest Apache project version number](http://httpd.apache.org/download.cgi) exceeds 2.x.x).
- In most Enterprise Linux-based distros (such as RHEL, CentOS), the package is called `httpd` (which stands for "HTTP Daemon") which is also what the Apache project calls itself ("Apache HTTP Server ('Apache' and 'httpd')" [source](http://httpd.apache.org/download.cgi)).
- In fact, practically every distro _but_ those that are Debian-baed (e.g. Arch Linux, OpenSUSE, Gentoo, etc.) just call it by the officially-supported name: `httpd`.

## Filepaths
### Settings
- Config
	- General
		- Debian (and its derivatives)
			- `/etc/apache2/`
			- `/etc/apache2/apache2.conf` - Main config file
			- Modules
				- `/etc/apache2/mods-enabled/php5.load` - Which PHP module to load
				- `/etc/apache2/mods-enabled/php5.conf` - Configuration for the chosen PHP module
		- Everything else
			- `/etc/httpd/`
			- `/etc/httpd/conf/httpd.conf` - Main config file
			- Modules
				- `/etc/httpd/conf.modules.d` - Module loading
				- `/etc/httpd/conf.modules.d/10-php.conf` - Which PHP module to load
				- `/etc/httpd/conf.d/` - Module confs
				- `/etc/httpd/conf.d/php.conf` - Configuration for the chosen PHP module
	- VirtualHosts
		- Debian
			- `/etc/apache2/sites-available/` - VirtualHost conf files that may or may not be currently active but are written and ready to be used
			- `/etc/apache2/sites-enabled/` - VirtualHost conf files that are currently in use
		- Everything else
			- This is left up to the user
- Web Root
	- `/var/www/`
		- This is the non-standard location but tends to be convention for many distros.
		- `/var/www/vhosts/` is a derivative of this one that sees fairly widespread usage.
	- `/srv/http/`
		- This is the standardized location per the [Filesystem Hierarchy Standard](http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html), specifically with regards to [this section](http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html#srvDataForServicesProvidedBySystem).

## HTTP Basic Auth
- [HTTP Basic Auth in Apache](http://tecadmin.net/how-to-secure-specific-url-in-apache/)
- [HTTP Basic Auth in Nginx](https://www.digitalocean.com/community/tutorials/how-to-set-up-http-authentication-with-nginx-on-ubuntu-12-10)

## Apache HTTP -> HTTPS Redirect
- http://stackoverflow.com/questions/4083221/how-to-redirect-all-http-requests-to-https
