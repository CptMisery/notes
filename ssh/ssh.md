# SSH
## Creating a Keypair
### Linux/MacOS
To create a keypair on MacOS or Linux, simply run the following command: `ssh-keygen -t rsa -b 4096 -C user@example.com_YYYY-MM-DD`. Note that this command is configurable in many ways and is simply intended as an all-around solid performer, balancing performance and security. The obvious customizations available in the above command include: excluding or changing the format of the key comment, using more bits, etc.

### Windows
1. Download and install PuTTY.
1. Run PuTTYGen. Set the Number of bits option to 4096 (or higher).
1. Press the Generate button. Move your mouse around in the rectangle to help generate randomness.
1. Save the private and public keys in PPK format in a safe, secure place.
1. Copy and paste the OpenSSH-formatted contents of the field labeled "Public key for pasting into authorized_keys file" into a file named like $USER_rsa.pub, and upload it here.


## Port forwarding
If you would like to access a specific port (let's say `3306` which is MySQL's default) on a remote server but it collides with a locally-running service (e.g. you also have a local MySQL server daemon running), this issue of "port conflict" can be resolved by running the following command on your local host: `ssh -fNg -L 3307:127.0.0.1:3306 user@server.com`.

This will cause all requests for `127.0.0.1:3307` to be internally (and seamlessly) redirected to `server.com:3306`.

For instance, if you have a web applicaiton and you want to use the database of a different server for some reason, you would simply run the above command and change the "host" to `localhost` and the "port" to `3307`. This will _actually_ request the MySQL instance on "host" `server.com` which is running on "port" `3306`, but your application need not know the difference.
