# Software & Tools
## Build Tools
- Jenkins (fully-fledged build tool)
- Gulp.js (build tool written on top of NodeJS geared towards web development)

## Databases
- MySQL Workbench

## Diagramming
- Draw.io
- XMind (for brainstorming specifically)
- BPMN (not a "tool" per se but kind of...it's essentially supercharged flowcharts)

## IDEs / Editors
- Atom
- PHPStorm (PHP + JS + CSS + web dev in general)
- Sublime Text
- Nano
- Vim

## Package Managers
- Bower (frontend web development)
- Composer (PHP)
- NPM (Node Package Manager for JS-specific assets that don't also fall under the umbrella covered by "Bower")

## Miscellaneous
- EditorConfig

### CLI
- `tmux`
	- See my `dotfiles` repo for more on this.
	- [Teamocil](https://github.com/remiprev/teamocil) (manage TMux layouts and such via YAML files)
- `zsh`
	- See my `dotfile` repo for more on this.

### CSS
- CSSComb

### HTTP
- `curl`
- HTTPie

### Javascript
- ESLint (code standards + linting)
- Jasmine (Unit Tests)

### Load Testing
- `ab` (not perfect...`siege` is better, but `ab` is good for quick, unofficial tests)
	- Examples:
		- `ab -n 500 -c 100 example.localhost` - Send 500 total requests with a concurrency of 100 at a time
		- `ab -n 25 -c 5 example.localhost` - 25 requests in total with concurrency of 5
		- `ab -n 100 -l -c 10 example.localhost` - 1-- total requests, 10 at a time, and `-l` prevents complaints about inconsistent load times (i.e. on dynamic web pages sometimes the page may take longer to generate than other times and this is to be expected).

### PHP
- Faker (convincing, randomly-generated fake data)
- PHPCodeSniffer (PHPCS) (code standards + linting)
- PHPMessDetector (PHPMD) (advanced code standards like cyclomatic dependency testing)
- PHPUnit (Unit Tests)

### SCSS
- SCSSLint

### VCS
- Git

### Web Applications (in general)
- PhantomJS + CasperJS add-on (integration tests)
