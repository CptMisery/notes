# Testing - Load Testing
- [Locust.io](http://locust.io/)
- [JMeter](http://jmeter.apache.org/) - for fancy stuff
- [Siege](https://www.joedog.org/siege-home/)
- [`ab` Apache HTTP server benchmarking tool](https://httpd.apache.org/docs/2.4/programs/ab.html) - Throw load at a single URL at a time...simple but effective if that's all you need. Example usage: `ab -n 1000 -c 10 example.com` for 1,000 requests, 10 at a time) 
