# MacOS - General

## System UI
- Holding down <kbd>Option</kbd> while hovering/clicking on various OS-level UI elements will often reveal alternative functionality.
	- Hint: Try this with the window controls and various Menu Bar items (particularly ones that OS X comes with).



## Screenshots
To change the path that screenshots are saved to by default, run this command: `defaults write com.apple.screencapture location [LOCATION]` where `[LOCATION]` might be something like `~/Pictures/Screenshots`.

This is particularly useful for those who heavily utilize the `CMD+SHIFT+{3,4}` screenshotting keyboard shortcuts.



## SSH
### Store SSH Key in System Keychain
- Abstract: For some reason, MacOS Sierra has broken persistent SSH key unlocking in the system keychain.
- Symptoms: You have to type in your SSH private key passphrase every single time you try to use the key.
- Solution:
	1. `ssh-add ~/.ssh/[your-key]`
		- Just ensure that it's been added to the system keychain.
	1. `ssh-add -K`
		- Tell the system to not only store the keypair, but _also_ store the key's _passphrase_. This allows you to unlock the keypair once per boot and then use it freely afterwards.
	1. In your `~/.ssh/config` file (which you're totally using already, right?), simply add `UseKeychain yes` to that `Host`'s rules.



## Three-finger drag/selection no longer working
For some reason this setting gets lost every so often. It is found here: `Preferences` > `Accessibility` > `Mouse & Trackpad` > `Trackpad Options...` > `Enable dragging` > `three finger drag`



## Window Management
- <kbd>Option</kbd> + `[Maximize window control]` = "Classic" Maximize functionality (make application fill screen in current workspace, while leaving the Dock/Menu Bar alone).
	- Without <kbd>Option</kbd>, the application will be placed in its own workspace in fullscreen mode.
- <kbd>Cmd</kbd>+<kbd>Tab</kbd> is for Application Switcher.
- <kbd>Ctrl</kbd>+<kbd>Tab</kbd> is for switching views within a specific app (e.g. Internet browser tabs).
	- If this shortcut does not work, <kbd>Cmd</kbd>+<kbd>]</kbd> / <kbd>Cmd</kbd>+<kbd>[</kbd> is a common alternative.



## Migrating to a Networked Account
If--for whatever reason--you have a new user account and would like to transfer _everything_ over from your old account to your new one (e.g. from one machine to another, or if you are going from a local account to an LDAP/"networked" account), here's what you should do:

### Different Old/New Usernames
1. In MacOS's `Users & Groups` management in the System Preferences, be sure to check the `Allow user to administer this computer` checkbox on the new user.
	- This ensures that the new user has `sudo`/`root` access.
1. Migrate data
	- Command: `sudo rsync -av /Users/[old-username]/ /Users/$(whoami)`
	- Note the trailing slash on the first path!
1. Migrate ownership from the old user to the new (choose one):
	1. Option #1
		- User: `sudo find "/Users/$(whoami)" -user [old-username] -exec chown $(whoami) {} \;`
		- Homebrew:
			- `sudo find "/usr/local/Cellar" -user [old-username] -exec chown $(whoami) {} \;`
			- `sudo find "/usr/local/Homebrew" -user [old-username] -exec chown $(whoami) {} \;`
	1. Option #2
		- System: `sudo find / -user [old-username] -exec chown $(whoami) {} \;`
		- Note: The idea behind this one, of course, is to just change _everything_ on the system from the old user to the new user **regardless** of where it is.


### New Username = Old Username
_If_, however, you are unlucky enough to have the following configuration:

- Before: `[old-username]`
- After: `[old-username]`

...then you will need to create a backup store of your old user's data elsewhere and change `/Users/[old-username]` to that path instead since the new network account collides with--and will potentially overwrite--the existing account.

In such situations, your workflow is as follows:

1. Create a backup: `rsync -av /Users/[old-username]/ [target]/[old-username]`
1. Restore your backup: `rsync -av --delete [target]/[old-username]/ /Users/[new-username]`
1. Transfer permissions: `sudo find / -user [old-username] -exec chown [new-username] {} \;`

> **NOTE:** Once your old account is gone, `[old-username]` will become a _user ID number_ rather than the human-readable `old_username`. To identify what ID number this is, find a file owned by your old user (e.g. via `ls -l`) and see what the value of the "owner" column is.
