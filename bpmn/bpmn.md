# BPMN (Business Process Model Notation)

## Examples
- [BPMN "Quick Guide"](http://www.bpmnquickguide.com/view-bpmn-quick-guide/) which is THE PERFECT "reference guide"
- [LucidChart's very quick overview](https://www.lucidchart.com/pages/bpmn/gateways)
- [The official standard](http://www.bpmn.org/)
